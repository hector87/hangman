﻿using HangMan.db;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Newtonsoft.Json;

namespace HangMan.db
{

    public class Words
    {
        public int Id { get; }
        public string Uuid { get; }
        public string Word { get; }

        public Words(int id, string uuid, string word) { 
            Id = id;
            Uuid = uuid;
            Word = word;
        }

    }

    public class Game
    { 
        public int Id { get; }
        public Words Word { get; }
        public bool Win { get; set; }
            
        public string LetterKnows { get; set; }

        public int Fault { get; set; }

        public Game(int id, Words word, bool win, string letterKnows, int fault)
        {
            Id = id;
            Word = word;
            Win = win;
            LetterKnows = letterKnows;
            Fault = fault;
        }


    
    }

    public sealed class DataBase
    {
        private static DataBase _instance = new DataBase();
        public Words[] data { get; set; }
        public Game[] Games { get; set; }
        public string[] Words { get; set; }

        private int _wordsLength { get; set; }

        static DataBase() { }
        private DataBase() 
        {
            WebClient client = new WebClient();
            client = new WebClient();
            // if you want get words from some webapp, below a example:
            //string downloadedString = client.DownloadString("https://random-word-api.herokuapp.com/all");
            //string[] randomWords = downloadedString.Replace("\"", "").Replace("\\", "").Replace("[", "").Split(',');
            string[] randomWords = GetDBWords();
            _wordsLength = randomWords.Length;
            data = new Words[_wordsLength];
            Games = new Game[_wordsLength];
            for (int i = 1; i <= _wordsLength; i++)
            {
                string uuid = Guid.NewGuid().ToString();
                Words w = new Words(i, uuid, randomWords[i - 1]);
                data[i-1] = w;
            }
        }
        
        public static DataBase Instance 
        {
            get
            {
                return _instance;
            }
        }

        public Game getAGame(int id)
        {
            // this method return a new game or a created a new one, 
            // value id guide us.
            if (id != 0)
            foreach (var item in Games)
            {
                if (item != null)
                {
                    if (item.Id == id)
                    {
                        return item;
                    }
                }
                
            }
            Random rnd = new Random();
            int randomValue = rnd.Next(_wordsLength - 1);
            Game result = new Game(randomValue, randomWord(), false, "", 0);
            Games[randomValue] = result;
            return result;


        }

        public Game? newGameByUuid(string uuid)
        {
            Words word = findWordByUuid(uuid);
            if (word == null)
            {
                return null;
            }
            else
            {
                Random rnd = new Random();
                int randomValue = rnd.Next(_wordsLength - 1);
                Game result = new Game(randomValue, word, false, "", 0);
                Games[randomValue] = result;
                return result;
            }

        }

        public Game? getGameById(int id)
        {

            foreach (var item in Games)
            {
                if (item != null)
                {
                    if (item.Id == id)
                    {
                        return item;
                    }
                }

            }

            return null;
        }

        public Words randomWord()
        {
            Random rnd = new Random();
            int rndWord = rnd.Next(_wordsLength - 1);
            return data[rndWord];
        }

        public Words? findWordByUuid(string uuid)
        {

            for(int i = 0; i < _wordsLength; i++)
            {
                if (data[i].Uuid == uuid)
                {
                    return data[i];
                }
            }

            return null;
        }

        public Words? findWordById(int id)
        {

            for(int i = 0; i < _wordsLength; i++)
            {
                if (data[i].Id == id)
                {
                    return data[i];
                }
            }

            return null;
        }

        private string[] GetDBWords()
        {
            using (StreamReader r = new StreamReader("db\\words.json")) 
            {
                string json = r.ReadToEnd();
                dynamic w = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                var words = w["words"];
                return words.ToObject<string[]>();
            }
            

            return new string[]{};
        }
    }

}

﻿using HangMan.db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;


namespace HangMan.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }
        public Words wordToShare { get; set; }
        public Game newGame { get; set; }
        public void OnGet(string uuid)
        {
            DataBase data = DataBase.Instance;
            if (uuid != null)
            {
                newGame = data.newGameByUuid(uuid);
                if (newGame == null)
                {
                    newGame = data.getAGame(0);
                }
            }
            else
            {
                newGame = data.getAGame(0);
            }
           
            wordToShare = data.randomWord();

        }
    }
}
﻿using HangMan.db;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HangMan.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        // GET: api/word/generate
        [Route("generate")]
        [HttpGet]
        public Words Get()
        {
            DataBase data = DataBase.Instance;
            return data.getAGame(0).Word;
        }
         // GET: api/word/update
        [Route("update")]
        [HttpGet]
        public IDictionary<string, object> Get(char letter, int gameId)
        {
            IDictionary<string, object> response = new Dictionary<string, object>() 
            {
                {"isFault", false},
                {"faults", 0},
                {"lose", false},
                {"win", false},
            };
            DataBase data = DataBase.Instance;
            Game aGame = data.getGameById(gameId);
            if (aGame != null)
            {
                string word = aGame.Word.Word;
                if (!word.ToLower().Contains(letter))
                {
                    aGame.Fault = aGame.Fault + 1;
                    response["isFault"] = true;
                    response["faults"] = aGame.Fault + 1;
                    if (aGame.Fault >= 6)
                    {
                        response["lose"] = true;
                        response.Add("word", aGame.Word.Word);
                    }
                    else
                    {
                        char[] wordResponse = new char[aGame.Word.Word.Length];
                        for (int i = 0; i < aGame.Word.Word.Length; i++)
                        {
                            if (aGame.LetterKnows.ToLower().Contains(aGame.Word.Word[i]))
                            {
                                wordResponse[i] = aGame.Word.Word[i];
                            }
                            else
                            {
                                wordResponse[i] = '_';
                            }
                        }
                        
                        response.Add("wordArray", wordResponse);
                    }
                } 
                else
                {
                    aGame.LetterKnows = aGame.LetterKnows + letter;
                    char[] wordResponse = new char[aGame.Word.Word.Length];
                    for (int i = 0; i < aGame.Word.Word.Length; i++)
                    {
                        if (aGame.LetterKnows.ToLower().Contains(aGame.Word.Word[i]))
                        {
                            wordResponse[i] = aGame.Word.Word[i];
                        }
                        else
                        {
                            wordResponse[i] = '_';
                        }
                    }
                    if (!wordResponse.Contains('_'))
                    {
                        response["win"] = true;
                    }
                    response.Add("wordArray", wordResponse);
                }

            }
            return response;
        }

        
    }

}

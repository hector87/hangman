﻿function generateNewWord(urlBase) {
    $.ajax({
        url: 'api/game/generate',
        data: {},
        type: 'get',
        success: function (data) {
            console.log(data)
            $("#wordRandom").html(data.word);
            const url = urlBase + "?uuid=" + data.uuid;
            $("#urlnewGame").attr('href', "?uuid=" + data.uuid);
            $("#urlnewGame").html(url);
        }
    });
}
function wordUpd(letter) {
    gameId = $('#gameId').val();
    $.ajax({
        url: 'api/game/update',
        data: {'letter': letter, 'gameId': gameId },
        type: 'get',
        success: function (data) {
            console.log(data);
            if (data.lose) {
                $('#imageMan').attr('src', 'hangmanpict/7.jpg');
                $('#playAgain').removeAttr('hidden');
                alert('You lose, the word is ' + data.word)
            } else {
                if (data.win) {
                    $('#' + letter).removeClass('btn-primary');
                    $('#' + letter).addClass('btn-success disabled');
                    for (let i = 0; i < data.wordArray.length; i++) {
                        if (data.wordArray[i] !== "_") {
                            $("#wordLetter" + (i + 1).toString()).html(data.wordArray[i])
                        }
                    }
                    $('#imageMan').attr('src', 'hangmanpict/8.jpg');
                    $('#playAgain').removeAttr('hidden');
                    alert("You win")
                } else {
                    if (!data.isFault) {
                        $('#' + letter).removeClass('btn-primary');
                        $('#' + letter).addClass('btn-success disabled');
                        for (let i = 0; i < data.wordArray.length; i++) {
                            if (data.wordArray[i] !== "_") {
                                $("#wordLetter" + (i + 1).toString()).html(data.wordArray[i])
                            }
                        }
                    } else {
                        $('#imageMan').attr('src', 'hangmanpict/' + data.faults + '.jpg');
                        $('#' + letter).removeClass('btn-primary');
                        $('#' + letter).addClass('btn-danger disabled');

                    }
                }
            }
        }
    });

}
